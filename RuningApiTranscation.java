
package org.wirabumi.gen.oez.ad_process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.service.OBDal;
import org.openbravo.scheduling.KillableProcess;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;

public class RuningApiTranscation extends DalBaseProcess implements KillableProcess {
	
	private static final Logger log4j = Logger.getLogger(RuningApiTranscation.class);
	private ProcessLogger logger;
	private List<String> transaction = null;
	private boolean killProcess = false;
	String Json = "";

	@Override
	protected void doExecute(ProcessBundle bundle) throws Exception {
		// TODO Auto-generated method stub
		logger = bundle.getLogger();
		transaction = getTrancationnBatch(bundle);
		postTrancation();
		while (true) {
		if (transaction.isEmpty()) {
			break;}}
	}
	

	@Override
	public void kill(ProcessBundle processBundle) throws Exception {
		// TODO Auto-generated method stub
		this.killProcess = true;
	}
	
	
	private void postTrancation(
//			String transaction_id, String product, String valuationType, String movementType , Integer movementQuantity, Integer acquisitionCost, String movementDate
			) {
		HttpClient httpClient = new DefaultHttpClient();
		
		try {
		    HttpPost request = new HttpPost("https://62b3e26b4f851f87f4610cf9.mockapi.io/transactionpr");
		    StringEntity params =new StringEntity("["+Json+"]");
		    request.addHeader("content-type", "application/json");
		    request.addHeader("Accept","application/json");
		    request.setEntity(params);
		    HttpResponse response = httpClient.execute(request);

		    // handle response here...
		}catch (Exception ex) {
		    // handle exception here
		} finally {
		    httpClient.getConnectionManager().shutdown();
		}

	}

	public List<String> getTrancationnBatch(ProcessBundle bundle) {
		String client = bundle.getContext().getClient();
		List<String> output = new ArrayList<>();
		String sql = 
				 " select	a.m_transaction_id  as \"correllationId\", "
				+ "		a.m_product_id as \"productCorrelationId\", "
				+ "		'MovingAverage' as \"valuationType\", "
				+ "		case when (a.movementtype='C-') then 'CustomerShipment' "
				+ "			 when (a.movementtype='C+') then 'CustomerReturn' "
				+ "			 when (a.movementtype='M-') then 'MovementOut' "
				+ "			 when (a.movementtype='M+') then 'MovementIn' "
				+ "			 when (a.movementtype='V+') then 'VendorReceipt' "
				+ "			 when (a.movementtype='V-') then 'VendorReturn' "
				+ "			 when (a.movementtype='V+' and a.movementqty<0) then 'VendorReturn' "
				+ "			 when (a.movementtype in ('I+', 'I-') and a.movementqty<0) then 'PhysicalInventoryOut' "
				+ "			 when (a.movementtype in ('I+', 'I-') and a.movementqty>=0) then 'PhysicalInventoryIn' "
				+ "			 else 'Unknown' "
				+ "			 end as \"movementType\", "
				+ "		case when (a.movementtype='V+') then 1 "
				+ "			 when (a.movementtype='I+') then 2 "
				+ "			 when (a.movementtype='C+') then 3 "
				+ "			 when (a.movementtype='M-') then 4 "
				+ "			 when (a.movementtype='M+') then 5 "
				+ "			 when (a.movementtype='C-') then 6 "
				+ "			 when (a.movementtype='V-') then 7 "
				+ "			 when (a.movementtype='I-') then 8 "
				+ "			 else 99 "
				+ "			 end as \"movementTypePriority\", "
				+ "		case when (a.movementtype='C-' and a.movementqty<0) then a.movementqty*(-1) "
				+ "			 when (a.movementtype='I-' and a.movementqty<0) then a.movementqty*(-1) "
				+ "			 when (a.movementtype='I+' and a.movementqty<0) then a.movementqty*(-1) "
				+ "			 when (a.movementtype='M-' and a.movementqty<0) then a.movementqty*(-1) "
				+ "			 when (a.movementtype='V-' and a.movementqty<0) then a.movementqty*(-1) "
				+ "			 when (a.movementtype='V+' and a.movementqty<0) then a.movementqty*(-1) "
				+ "			 else a.movementqty "
				+ "			 end as \"movementQuantity\", "
				+ "		case when (a.movementtype='V+' and a.movementqty>=0) then  "
				+ "				coalesce( "
				+ "				 (select sum(y.linenetamt) as acquisitionCost  "
				+ "				 from m_matchinv x "
				+ "				 left join c_invoiceline y on y.c_invoiceline_id = x.c_invoiceline_id "
				+ "				 where x.m_inoutline_id = a.m_inoutline_id), "
				+ "				 (select sum(v.linenetamt) as acquisitionCost "
				+ "				 from m_matchpo u "
				+ "				 left join c_orderline v on v.c_orderline_id = u.c_orderline_id "
				+ "				 where u.m_inoutline_id =a.m_inoutline_id) "
				+ "				) "
				+ "			 when (a.movementtype in ('I-', 'I+')) then "
				+ "			 	(select coalesce(cost, 0::numeric) from m_inventoryline x where x.m_inventoryline_id = a.m_inventoryline_id ) "
				+ "			 end as \"acquisitionCost\", "
				+ "		TO_CHAR(a.movementdate :: timestamp, 'yyyy-MM-ddThh:mm:ss') as \"movementDate\", "
				+ "		'NotCalculated' as \"costingStatus\", "
				+ "		a.m_movementline_id as \"movementOutCorrelationId\" "
				+ "     from m_transaction a"
				+ "		where a.ad_client_id = '"+client+"'"
			    + "	and a.isprocessed = 'N' "
				+ "  ";
				
		java.sql.Connection conn = OBDal.getInstance().getConnection();
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			int n = 0;
			while (rs.next()) {
				
				
				if(n != 0) {
					Json=Json+",";
				};
				
				Json = Json+"{ \"product\": "
									+" { \"productId\":\""+rs.getString("productCorrelationId")+"\", "
									+" \"valuationType\":\""+rs.getString("valuationType")+"\"}, "
						+ " \"id\":\""+rs.getString("correllationId")+"\", "
						+ " \"movementType\":\""+rs.getString("movementType")+"\", "
						+ " \"movementQuantity\":"+rs.getString("movementQuantity")+", "
						+ " \"acquisitionCost\":"+rs.getString("acquisitionCost")+", "
						+ " \"movementDate\":\""+rs.getString("movementDate")+"\"}";
				n = n+1;
				if (killProcess) {
					throw new OBException("Process killed");
				}
			}
		} catch (Exception e) {
			logger.logln("Error message = " + e.getMessage() + "/nCause = " + e.getCause());
		}
		return output;
	}
}
